#!/bin/bash

# This script needs one argument that will be used as the suffix to append to
# the "bandwidth" and "peakflops" output files.

# Setup test for bandwidth
test_bandwidth="load_avx"
#sizes_bandwidth=( 20kB 34kB 258kB 32002kB ) # miriel
sizes_bandwidth=( 20kB 34kB 1004kB 24760kB ) # bora

# Setup tests for peak FLOP/s
tests_peakflops=( "peakflops" "peakflops_sse" "peakflops_avx" "peakflops_avx_fma" )


################################################################################
#                      Measure bandwidth and peak FLOP/s
################################################################################

# Generate output file names
suffix=$1
outFile_bandwidth="bandwidth$suffix"
outFile_peakflops="peakflops$suffix"

# Measure bandwidth
./measure_bandwidth.sh $test_bandwidth ${sizes_bandwidth[@]} $outFile_bandwidth

# Measure peak FLOP/s
./measure_peakflops.sh ${tests_peakflops[@]} $outFile_peakflops

# Show output
cat $outFile_bandwidth
cat $outFile_peakflops
