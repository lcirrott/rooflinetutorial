#!/bin/bash

# The first script argument is the test to be run
test=$1
# The following four arguments are the data sizes in L1,L2,L3,DRAM
sizes=( $2 $3 $4 $5 )
# Sixth argument is the output file
outFile=$6
echo -n "" > $outFile

echo "# Size   MByte/s" >> $outFile
for size in ${sizes[@]}
do
  echo -n "$size   " >> $outFile
  likwid-bench -t $test -W N:$size:1 | grep "MByte" | awk '{ print $2 }' >> $outFile
done
