#!/bin/bash

# All but the last argument are tests to be run
while (( $# > 1 ))
do
  tests+=( $1 )
  shift
done

#tests=( "peakflops" "peakflops_sse" "peakflops_avx" "peakflops_avx_fma")
# Last argument is the output file
outFile=$1
echo -n "" > $outFile

echo "# Test   MFLOP/s" >> $outFile
for test in ${tests[@]}
do
  echo -n "$test   " >> $outFile
  likwid-bench -t $test -W N:768kB:1 | grep "MFlop" | awk '{ print $2 }' >> $outFile
done
