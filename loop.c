#include <stdlib.h>
#include <string.h>

/* LIKWID headers, definitons and macros */
#ifdef LIKWID_PERFMON
#include <likwid-marker.h>
#else
#define LIKWID_MARKER_INIT
#define LIKWID_MARKER_THREADINIT
#define LIKWID_MARKER_SWITCH
#define LIKWID_MARKER_REGISTER(regionTag)
#define LIKWID_MARKER_START(regionTag)
#define LIKWID_MARKER_STOP(regionTag)
#define LIKWID_MARKER_CLOSE
#define LIKWID_MARKER_GET(regionTag, nevents, events, time, count)
#endif

/* My definitions */
#define n 2048
#define LOOP( var, begin, end ) for ( var = begin; var < end; var++ )
typedef double Type;

/**
 * Multiply two matrices: C = A * B
 */
void multiply_ijk( Type* A, Type* B, Type* C ) {
  int i, j, k;

  /* Initialize the result */
  memset( C, 0, sizeof(Type) * n * n );

  /* Perform the multiplication */
  LOOP( i, 0, n ) {
    LOOP( j, 0, n ) {
      /* Use a local variable to compute the sum, so it will be stored on a
       * processor register and no line of C will be loaded in the innermost
       * loop. */
      Type sum = 0.0;
      LOOP( k, 0, n ) {
        sum += A[i*n+k] * B[k*n+j];
      }
      C[i*n+j] = C[i*n+j] + sum;
    }
  }
}

/**
 * Multiply two matrices: C = A * B
 */
void multiply_ikj( Type* A, Type* B, Type* C ) {
  int i, j, k;

  /* Initialize the result */
  memset( C, 0, sizeof(Type) * n * n );

  /* Perform the multiplication */
  LOOP( i, 0, n ) {
    LOOP( k, 0, n ) {
      Type alpha = A[i*n+k];
      LOOP( j, 0, n ) {
        C[i*n+j] = C[i*n+j] + alpha * B[k*n+j];
      }
    }
  }
}

/**
 * Small program to test matrix operations.
 */
int main( int argc, char* argv[] ) {

  LIKWID_MARKER_INIT;
  LIKWID_MARKER_THREADINIT;


  Type* A;
  Type* B;
  Type* C;

  /* Allocate matrices */
  A = (Type*) malloc( n * n * sizeof(Type) );
  B = (Type*) malloc( n * n * sizeof(Type) );
  C = (Type*) malloc( n * n * sizeof(Type) );

  /* ... matrices are not initialized, as we are not interested in real results
   * in this test... */

  /* Perform matrix product C = A * B */
  LIKWID_MARKER_START("Multiply ijk");
  multiply_ijk( A, B, C);
  LIKWID_MARKER_STOP("Multiply ijk");

  LIKWID_MARKER_START("Multiply ikj");
  multiply_ikj( A, B, C);
  LIKWID_MARKER_STOP("Multiply ikj");

  /* Free memory */
  free( A );
  free( B );
  free( C );

  LIKWID_MARKER_CLOSE;

  return EXIT_SUCCESS;
}
