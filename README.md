[![License: CC BY 4.0](https://img.shields.io/badge/License-CC_BY_4.0-lightgrey.svg)](https://creativecommons.org/licenses/by/4.0/)

All the content of this repository is licensed under the Creative Commons Attribution 4.0 International license (https://creativecommons.org/licenses/by/4.0/).

# Preface
The purpose of these short notes is to provide a tutorial on the _roofline model_ performance analysis, a technique which explores computational and memory bottlenecks of software application in a unified representation.
- The target public are computational scientists with academic backgrounds in applied mathematics, engineering, physics or chemistry and other fields that are not specifically computer science or microelectronics.
- The aim is to undestand the principles and to be able to run a roofline analysis autonomously, but reading this material, testing the commands and playing with the attached scripts should take no more than three hours.

> :warning: **Disclaimer 1:**
I will only cite the minimal knowledge about computer architecture that is needed to understand the performance analysis technique at hand, without digging into details that are left to better works written by experts of these topics.

> :warning: **Disclaimer 2:**
The author will not discuss software tools for software environments other than Linux operating systems (OS's). In the HPC market, Linux is not a niche: 100% of the computers in the Top500 list run a Linux-family OS (https://www.top500.org/statistics/list/). If we want to use state-of-the-art tools in HPC, it is a pedagogical choice to learn how to use Linux-family OS's.

## Table of contents
1. [Performances and the cache hierarchy](#performances-and-the-cache-hierarchy)\
  1.1. [Multicore processors](#1-multicore-processors)\
  1.2. [Memory hierarchy](#2-memory-hierarchy)\
  1.3. [Cache lines and locality](#3-cache-lines-and-locality)
2. [The roofline model](#the-roofline-model)
3. [Tutorial](#tutorial)\
  3.1. [Roofline analysis with Intel Advisor](#roofline-analysis-with-intel-advisor)\
  3.2. [Roofline analysis with LIKWID](#roofline-analysis-with-likwid)

# Performances and the cache hierarchy
To understand software performances nowadays, it is important to recall some basics about the recent evolutions in computer architectures. One observation concerns CPUs tecnology, another concerns memory tecnology, a third one concerns how data are loaded into memory.

## 1. Multicore processors
It is classical to start discuss this topic from Moore's observation (1965) that transistor count nearly doubles every two years.
![Moore](https://assets.ourworldindata.org/uploads/2022/03/Transistor-Count-over-time.png)

Nonetheless, it is important to look at how this growth has been sustained by the microprocessors industry (image provided by Karl Rupp under Creative Commons Attribution 4.0 International Public License, at https://github.com/karlrupp/microprocessor-trend-data/).
![Rupp](https://raw.githubusercontent.com/karlrupp/microprocessor-trend-data/master/50yrs/50-years-processor-trend.png)
This picture shows that in the last two decades the increase 
in transistor count has been performed with an increase in the number of CPU cores on the same socket, while the core frequency has stopped to increase due to the related increase in power (and heating).

:white_check_mark: The first conclusion we can draw is that in order to achieve maximal performances on a processor socket, we _need_ to exploit its parallelism.

## 2. Memory hierarchy
Once we have a fast (multicore) processor, it is also important to have fast memory because we don't want the CPU core to stay idle while waiting for data to be fetched from memory. We will only consider here _main_ memory, which is _volatile_, _random-access_ memory (RAM).
The following image resumes a typical "memory hierarchy" (also including secondary, non-volatile storage for completeness) with a classification based on speed (and cost).

![hierarchy](https://upload.wikimedia.org/wikipedia/commons/0/0c/ComputerMemoryHierarchy.svg)

Two types of RAM are typically available, _static_ RAM and _dynamic_ RAM. Due to technological reasons, the first is much faster than the second, but also more expensive. As a compromise between cost and speed, a _cache memory hierarchy_ is proposed on modern architectures. Between the small processor registers on each CPU core and a large DRAM main memory shared among CPU cores, several levels of SRAM cache memory are introduced (typically ranging from L1 to L3), with decreasing speed and size.

The following image (obtained with the `lstopo` command) shows an actual layour of CPU cores and cache memory on an Intel Haswell architecture.
![lstopo](images/lstopo_miriel.png)
This computing node contains two sockets, each with 12 cores. Each bank of DRAM (4 in total) is shared by 6 cores. L3 cache is also shared by groups of 6 cores, while L2 and L1 caches are specific for each core (L1d and L1i stay for "data" and "instruction" L1 caches).

Cache sizes are also shown: 32 kB for L1, 256 kB for L2, 15 MB for L3. Some benchmarks can give us an idea of the latency to fetch data from each cache level (see for example https://uops.info/cache.html). Typical values are about 4 clock cycles for L2, about 12 cycles for L2, and about 38 cycles for L3. Fetching data from DRAM typically takes hundreds of clock cycles. Since some memory levels are shared while others are not, having a process/thread fetching data from a memory "outside" its range becomes a costly operation that should be avoided.

:white_check_mark: A second conclusion is that binding/pinning processes/threads to a specific core is necessary to prevent having a process/thread move to another core and reload its data in all its cache levels from a main memory maybe on another socket.

## 3. Cache lines and locality
Given this picture and these data about memory speed, it is clear that we would like to maximize the usage (and re-usage) of data in fast cache levels.
To achieve this, we have to understant how caches are used.
- When the core needs some data, it normally looks for it in its register, than in L1, L2, L3, and finally in DRAM. If the data is found in a cache we call it a _cache_hit_, otherwise a _cache miss_. A piece of data persists in each cache level when it is fetched from DRAM all the way up to L1 if the cache is _inclusive_.
- Cache memory is read by _lines_. This again is dictated by the memory tecnology. Modern cache lines are typically 64 B wide. This means that every time a piece of data is needed by the processor, it is this piece of data plus its contiguous neighbours in DRAM that are stored in the cache, up to the minimum multiple of 64 B that is able to contain the size of the piece of data.

An important remark is that scientific software often show some kind of _spatial locality_ or _temporal locality_ in the usage pattern of its data. This means that neighbouring data (in memory) are often used by subsequent operations, or that the same data is reused by several subsequent operations before being discarded. This offers the potential to optimally exploit the cache levels, provided that we are able to tailor our algorithm to work on all data it has in L1 before loading a new line from L2, and so on.

Take as exemple the computation of some integrals on a finite element mesh: geometrically neighbouring elements are often processed one ofter the other, and each integral computation reuses the coordinates of the points shared among them. Especially with cartesian meshes, some care can be taken to try to translate this geometrical contiguity into memory contiguity, and maximize locality.

The kind of modifications that could be made on the algorithm at hand and to the way the data are stored also depend on the programming language. A classical example stems from the comparison between C and Fortran: multi-dimensional arrays are stored in row-major order in C (see for example [its C23 standard draft](https://www.open-std.org/jtc1/sc22/wg14/www/docs/n2310.pdf) at paragraph _6.5.2.1 Array subscripting_), while they are stored in column-major order in Fortran (see the [community documentation](https://fortran-lang.org/en/learn/best_practices/multidim_arrays/)).

:white_check_mark: A third conclusion is that data alignment and memory access patterns should be carefully studied in order to maximize data locality and reduce cache misses.

# The roofline model
The _roofline model_ is an approximated model to analyse the performances of a software application, or a precise function or algorithm referred to as computational _kernel_. It aims at locating the performances of the application in a 2D plot where the x-axis is the _operational intensity_ $`I`$, defined as the ratio between the floating point operations $`O`$ (FLOP) performed by the kernel and the size $`S`$ (B) of the data loaded/stored by it
```math
I = \frac{ O \text{(FLOP)} }{ S \text{(B)} }
```
and the y-axis is the performance $`P`$ (GFLOP/s) defined as the number of floating point operations performed per second (often measured in GFLOP/s).

The performance limits of the machine are traduced in boundaries, or roofs, that delimitate the region of performances that are achievable by an application on that machine.
- One limit is the maximum FLOP/s achievable on that machine, producing an horizontal line,
- Another limit is the bandwidth of each memory level. Since the processor cannot perform operations if data is not ready, the performances of a computational kernel cannot be faster than what is allowed by the memory bandwidth. This limit is given by the product of the memory bandwidth $`W`$ (GB/s) with the operational intensity $`I`$ (FLOP/B) of the kernel, giving a line passing through the origins of the axes.

Kernel performances are drawn as single markers on the plot.

An example is given in the following picture, for an Intel Cascadelake architecture.
![roofline](images/roofline.png)

Several bounds are shown, since several peak performances can be achieved depending on the instruction set used, and since each memory and cache level as a different bandwidth.

1. On the right part of the picture, the performance bounds are set by the computational speed of the processor. If a kernel falls under these roofs, it is said to be **compute bound**, and developers should think about how to make it perform more operations with the same data (for example, SIMD vectorization).
2. On the left part of the picture, the performancs bounds are set by the memory bandwidth. If a kernel falls under these roofs, it is said to be **memory bound**. If it doesn't touch one of the roof, developers can either try to improve its FLOP/s or its operational intensity. When the kernel touches a memory roof, its performance cannot be improved unless its operational intensity is improved in the first place.

:white_check_mark: The performances of a compute-bound kernel can be improved, for example, by exploiting SIMD vectorization.

:white_check_mark: Improving the operational intensity of a kernel, meaning the FLOP performed per byte of data, is related to the memory access patterns, thus the locality of the data.

:white_check_mark: The memory-bound character of a kernel can be confirmed and better understood by measuring the ratio between cache misses and cache hits for each cache level.

# Tutorial
Two software tools for performance analysis offering the possibility to compute a roofline model will be presented. _Intel Advisor_ is a proprietary tool available on many cluster. _LIKWID_ ("Like I Know What I'm Doing") is a free (_libre_) tool mainly developed by the Erlangen National High Performance Computing Center.

The experiences are shown on the PlaFRIM cluster (https://www.plafrim.fr/), but standard scheduling and packaging tools are employed (Slurm, Environment Modules, Guix).
An interactive resource allocation is suggested for explore the tools. In order to ask for exclusive access to a `bora` node, for three hours, it goes along the line of:
```
salloc -C bora --time=03:00:00 --exclusive
```
When the resources are ready, the name of the available node will be prompt in the terminal. Connect to it with `ssh -X` followed by its name.

## The application
The C source code [loop.c](loop.c) contains a simple matrix multiplication, which analytically reads:
```math
C_{ij} = C_{ij} + A_{ik} B_{kj} \qquad \forall i,j,k = 1,\ldots,n
```
Numerically, this product can be computed according to different orderings of the three indices. We study two implementations, one with the naive $`(i,j,k)`$ ordering, and a better one with the $`(i,k,j)`$ ordering.

#### $`(i,j,k)`$ ordering
It is important to see how the innermost iteration $` k `$ operates:
```math
C_{ij} = C_{ij} + A_{i*} B_{*j} \qquad \forall i,j = 1,\ldots,n
```
<div align="center">
<i> Every entry of $`C`$ is computed as a row-column product. </i>
</div>

Or, graphically:
```
   j                 j
 [    ]    [    ]  [ |  ]
 [    ]    [    ]  [ |  ]
i[ *  ] = i[----]  [ |  ]
 [    ]    [    ]  [ |  ]
```
Since data in C are stored by row (in Fortran it would be by column), this means that a cache miss is produced at each innermost iteration, since it involves different rows of $`B`$.

This kernel needs, for every innermost iteration, 2 FLOP (the multiplication of $` A_{ik} `$ with $` B_{kj} `$, and the sum of the result to the previous value of $` C_{ij} `$) and the 2 _double_ values $` A_{ik} `$ and $` B_{kj} `$ ($` C_{ij} `$ being reused during the innermost loop, the compiler can decide to store it in a processor register). Theoretically, the operational intensity is thus:
```math
I = \frac{ 2\ \mathrm{FLOPS} }{ 2 \times 8\ \mathrm{bytes} } = \frac{1}{8}\ \mathrm{FLOPS/byte} = 0.125\ \mathrm{FLOPS/byte}
```

:warning: Keep in mind that the compiler, while vectorizing, can transform the loops and thus the observed operational intensity can be slightly different from the theoretical one.

#### $`(i,k,j)`$ ordering
This ordering is smarter than the previous one, because the innermost iteration $` j `$ operates as:
```math
C_{i*} = C_{i*} + A_{ik} B_{k*} \qquad \forall i,k = 1,\ldots,n
```
<div align="center">
<i> Every row i of C is incremented with a row k of B, scaled by the entry (i,k) of A. </i>
</div>

The vector operation in the innermost iteration is also known with the acronym _DAXPY_ (**d**ouble precision **a**lpha by **x** **p**lus **y**, as it solves an equation of the kind $` \vec y = \alpha \vec x + \vec y `$).

Graphically:
```
             k
 [    ]    [    ]  [    ]
 [    ]    [    ] k[----]
i[----] = i[ *  ]  [    ]
 [    ]    [    ]  [    ]
```
This ordering will produce less cache misses, since no matrix row index is changed in the innermost iteration.

This kernel needs, for every innermost iteration, 2 FLOP again (multiplicating $` A_{ik} `$ with $` B_{kj} `$, and adding the result to $` C_{ij} `$) with 3 data transfers: loading 2 _double_ values $` C_{ij} `$ and $` B_{kj} `$ ($` A_{ik} `$ being reused in the innermost loop, the compiler can decide to store it in a processor register), and writing the result $` C_{ij} `$.  Theoretically, the operational intensity is thus:
```math
I = \frac{ 2\ \mathrm{FLOPS} }{ 3 \times 8\ \mathrm{bytes} } = \frac{1}{12}\ \mathrm{FLOPS/byte} = 0.0833\ \mathrm{FLOPS/byte}
```

:warning: Again, please keep in mind that this analytic computation does not take into account compiler vectorization.


## Roofline analysis with Intel Advisor

### Preparing the environment
This procedure is specific to PlaFRIM. Load the Advisor module and run: 
```
module load compiler/gcc/9.3.0
module load compiler/intel/2020_update4
alias advixe-cl="$CMPL_DIR/advisor/bin64/advixe-cl"
alias advixe-gui="$CMPL_DIR/advisor/bin64/advixe-gui"
```
Compile:
```
icc -O3 -g loop.c -o program_loop -fno-inline -qopt-report -xCORE-AVX2
```
> :warning: If the Intel compiler module is not available, for this exercise you can use another Intel Advisor module with the default version of GCC:
> ```
> module load intel/vtune-advisor
> gcc -o program_loop loop.c -lm -g -O3 -march=native -mavx2 -mno-fma
> ```

### Run the analysis
```
advixe-cl --collect survey --ignore-checksums --project-dir ./results_loop0 --no-trip-counts -- ./program_loop
advixe-cl --collect tripcounts --ignore-checksums --project-dir ./results_loop0 --flop -- ./program_loop
```
Visualize the results:
```
advixe-gui results_loop0/results_loop0.advixeproj
```
Here the result:
![roofline_advisor](images/roofline_advisor_bora.png)

In the left panel, re-run the analysis "for all memory levels" to get an insight into the cache usage:
![cache_advisor](images/cache_advisor_bora.png)


## Roofline analysis with LIKWID

LIKWID actually offers a suite of tools, for many purposes. Their wiki page https://github.com/RRZE-HPC/likwid/wiki is well structured and offers a primer for each of them.
We will just focus on:
- `likwid-topology` for the exploration of hardware topology,
- `likwid-perfctr` for measuring performances through hardware counters.
- `likwid-bench` for running assembly benchmarks,

LIKWID leaves you complete flexibility about which benchmarks to use for measuring hardware roofs, and which counters to use for measuring the application performances. Thus, running a roofline analysis typically involves two steps:
- measuring peak FLOP/s and memory bandwidth for a combination of interest of instruction sets and memory levels with `likwid-bench`,
- measuring some interesting performance counters on the application run with `likwid-perfctr`.


### Preparing the environment
Since LIKWID is free, we can find it in _Guix_, a free package manager focused on reproducibility. We can find it in the Guix-HPC channel (https://gitlab.inria.fr/guix-hpc/guix-hpc), which can be added according to the procedure in https://guix.gnu.org/manual/en/html_node/Specifying-Additional-Channels.html. However, on PlaFRIM this channel is already activated by default.

We can build an interactive container isolated from previous environment variables and containing only LIKWID with the following command:
```
guix shell --pure likwid
```
Anyway, since no other program is made available in container by default, it is practical to add some other useful tools for the next experiences:
```
guix shell --pure likwid less gcc-toolchain grep gawk coreutils gnuplot
```
All the following experiences can be performed in this shell.

Since we will use the marker API of LIKWID to measure specific parts of the program, compile it with:
```
gcc -o program_loop loop.c -lm -DLIKWID_PERFMON -I$GUIX_ENVIRONMENT/include -L$GUIX_ENVIRONMENT/lib -llikwid -O3 -march=cascadelake -mavx2 -mno-fma
```
> You can check if any loop vectorization has been performed by passing the `-fopt-info` option (among others) to GCC.

:warning: I am purposely not using the best instruction set available on the machine, in order to eventually evaluate the benefits of higher instruction sets later on.


### Exploring the hardware -- `likwid-topology`
Exploring the hardware topology can be done simply running
```
likwid-topology
```
or
```
likwid-topology -c
```
if more information about caches are desired.

Cache sizes, in particular, will be used for choosing the data sizes for the benchmarks needed for the memory roofs computation.

The topology results can be compared with those provided by other tools like _hwloc_ by its command
```
lstopo
```

### What can I measure? -- `likwid-perfctr`
The `likwid-perfctr` tool gives access to the value of hardware counters for the application at hand.
Its options can be inspected with
```
likwid-perfctr -h
```
The list of available hardware events is obtained with
```
likwid-perfctr -e
```
This list can seem a bit overwhealming. For this reason, the most used counters are grouped in so-called _performance groups_, which can be inspected with
```
likwid-perfctr -a
```
A specific performance group can be asked with the `-g` option. The properties of the group, included the name of the performance counters and the formulas to compute performances, can be inspected with a subsequent `-H` option. For example, for the `FLOPS_AVX` group, the command:
```
likwid-perfctr -g FLOPS_AVX -H
```
gives:
```
Group FLOPS_AVX:
Formulas:
Packed SP [MFLOP/s] = 1.0E-06*(AVX_INSTS_CALC*8)/runtime
Packed DP [MFLOP/s] = 1.0E-06*(AVX_INSTS_CALC*4)/runtime
-
Packed 32b AVX FLOP/s rates. Approximate counts of AVX & AVX2 256-bit instructions.
May count non-AVX instructions that employ 256-bit operations, including (but
not necessarily limited to) rep string instructions that use 256-bit loads and
stores for optimized performance, XSAVE* and XRSTOR*, and operations that
transition the x87 FPU data registers between x87 and MMX.
Caution: The event AVX_INSTS_CALC counts the insertf128 instruction often used
by the Intel C compilers for (unaligned) vector loads.
```
Similarly, for the group `DATA`, the command:
```
likwid-perfctr -g DATA -H
```
gives:
```
Group DATA:
Formulas:
Load to store ratio = MEM_UOPS_RETIRED_LOADS/MEM_UOPS_RETIRED_STORES
-
This is a metric to determine your load to store ratio.
```

For example, running the `FLOPS_AVX` group on our application pinned on core 0 of socket 0 (`-C` option) and with the marker API (`-m` option) is done by:
```
likwid-perfctr -C S0:0 -g FLOPS_AVX ./program_loop
```

LIKWID also has a marker API allowing to instrument specific portions of the code (see https://github.com/RRZE-HPC/likwid/wiki/TutorialMarkerC for a C tutorial), which has been used in our source file [loop.c](loop.c) to instrument the $`(i,j,k)`$ and $`(i,k,j)`$ kernels singularly. Since it requires including/linking the LIKWID API and this last is available in our Guix environment, we have to compile our program as:
```
gcc -o program_loop loop.c -lm -DLIKWID_PERFMON -I$GUIX_ENVIRONMENT/include -L$GUIX_ENVIRONMENT/lib -llikwid -O3 -march=cascadelake -mavx2 -mno-fma
```
> You can check if any loop vectorization has been performed by passing the `-fopt-info` option (among others) to GCC.

Then the markers will be recognized once the `-m` option is used:
```
likwid-perfctr -m -C S0:0 -g FLOPS_AVX ./program_loop
```


### What benchmarks can I compare with? -- `likwid-bench`

Several standard benchmarks are available through `likwid-bench`. Since these benchmarks are written in assembly, the correct instructions to target maximum performances are directly used.

Measurements do not involve the usage of hardware counters. As explained in the documentation, this property can be exploited to validate the results of hardware counters on the current machine by running `likwid-perfctr` on the `likwid-bench` test case. 

These benchmarks are extremely useful to measure the architecture roofs. For example, we can get an estimate of the L2 bandwidth with a `load_avx` benchmark, with data size 85 kB (fitting L2) using one thread:
```
likwid-bench -t load_avx -W N:85kB:1 | grep "MByte"
```
The script [measure_bandwidth.sh](measure_bandwidth.sh) automizes the computation the bandwidth of different memory levels by taking different data sizes.

Similarly, we can get an estimate of the peak FLOP/s with the `peakflops` benchmarks available for different instruction sets:
```
likwid-bench -t peakflops_avx -W N:768kB:1
```
The script [measure_peakflops.sh](measure_peakflops.sh) automizes this task by running the benchmark for different instruction sets.

It is important to notice that peak FLOP/s can be compared with analytical predictions.
```math
[clock\ frequency] \times [nb.\ cores] \times [nb\ of\ data\ fitting\ in\ register] \times [vector\ processing\ units] \times [FMA\ units\ vectorization]
```
For example, on Bora (Intel Cascadelake), at a frequency of 2.5 GHz and with 1 thread, we get:
- with AVX instruction set (128 B registers):        [ 2.5 ] x [1] x [2] x [2] x [1] = 10 GFLOP/s
- with AVX instruction set (254 B registers):        [ 2.5 ] x [1] x [4] x [2] x [1] = 20 GFLOP/s
- with AVX FMA instruction set (254 B registers):    [ 2.5 ] x [1] x [4] x [2] x [2] = 40 GFLOP/s
- with AVX512 instruction set (512 B registers):     [ 2.5 ] x [1] x [8] x [2] x [1] = 40 GFLOP/s
- with AVX512 FMA instruction set (512 B registers): [ 2.5 ] x [1] x [8] x [2] x [2] = 80 GFLOP/s

and so on if we also increase the number of used cores.

The script [measure_roofline.sh](measure_roofline.sh) is a wrapper for the two previous scripts.


#### Validation
You can directly check some benchmarks against the accuracy results for some tested architectures, when these data are provided. `miriel` nodes have a Haswell-EP architecture, which is tested:
https://github.com/RRZE-HPC/likwid/wiki/AccuracyHaswellEP

For exemple, you can run the assembly benchmark `load`:
```
likwid-bench -t load -W N:85kB:1 | grep "MByte"
```

and validate performance counters with:
```
likwid-perfctr -m -C S0:0 -g L2 likwid-bench -t load -W N:85kB:1 | grep "L2 bandwidth"
```


### Putting all together -- Roofline analysis

The machine roofs can be estimated with the script [measure_roofline.sh](measure_roofline.sh), passing to it the suffix you want it to append to its output file names "peakflops[suffix]" and "bandwidth[suffix]". For example, if you run `./measure_roofline.sh Bora` it will produce two output files "peakflopsBora" and "bandwidthBora".

The two kernels can be measured with
```
likwid-perfctr -m -C S0:0 -g FLOPS_DP ./program_loop
likwid-perfctr -m -C S0:0 -g DATA ./program_loop
```

Here the results for the two commands:
```
--------------------------------------------------------------------------------
CPU name:	Intel(R) Xeon(R) Gold 6240 CPU @ 2.60GHz
CPU type:	Intel Cascadelake SP processor
CPU clock:	2.59 GHz
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
Region Multiply ijk, Group 1: FLOPS_DP
+-------------------+------------+
|    Region Info    | HWThread 0 |
+-------------------+------------+
| RDTSC Runtime [s] |  28.642850 |
|     call count    |          1 |
+-------------------+------------+

+------------------------------------------+---------+-------------+
|                   Event                  | Counter |  HWThread 0 |
+------------------------------------------+---------+-------------+
|             INSTR_RETIRED_ANY            |  FIXC0  | 51581610000 |
|           CPU_CLK_UNHALTED_CORE          |  FIXC1  | 74106420000 |
|           CPU_CLK_UNHALTED_REF           |  FIXC2  | 74112750000 |
| FP_ARITH_INST_RETIRED_128B_PACKED_DOUBLE |   PMC0  |           0 |
|    FP_ARITH_INST_RETIRED_SCALAR_DOUBLE   |   PMC1  | 17184060000 |
| FP_ARITH_INST_RETIRED_256B_PACKED_DOUBLE |   PMC2  |           0 |
| FP_ARITH_INST_RETIRED_512B_PACKED_DOUBLE |   PMC3  |           0 |
+------------------------------------------+---------+-------------+

+----------------------+------------+
|        Metric        | HWThread 0 |
+----------------------+------------+
|  Runtime (RDTSC) [s] |    28.6428 |
| Runtime unhalted [s] |    28.5728 |
|      Clock [MHz]     |  2593.3779 |
|          CPI         |     1.4367 |
|     DP [MFLOP/s]     |   599.9424 |
|   AVX DP [MFLOP/s]   |          0 |
|  AVX512 DP [MFLOP/s] |          0 |
|   Packed [MUOPS/s]   |          0 |
|   Scalar [MUOPS/s]   |   599.9424 |
|  Vectorization ratio |          0 |
+----------------------+------------+

Region Multiply ikj, Group 1: FLOPS_DP
+-------------------+------------+
|    Region Info    | HWThread 0 |
+-------------------+------------+
| RDTSC Runtime [s] |   3.173665 |
|     call count    |          1 |
+-------------------+------------+

+------------------------------------------+---------+-------------+
|                   Event                  | Counter |  HWThread 0 |
+------------------------------------------+---------+-------------+
|             INSTR_RETIRED_ANY            |  FIXC0  | 12943650000 |
|           CPU_CLK_UNHALTED_CORE          |  FIXC1  |  8207609000 |
|           CPU_CLK_UNHALTED_REF           |  FIXC2  |  8207804000 |
| FP_ARITH_INST_RETIRED_128B_PACKED_DOUBLE |   PMC0  |           0 |
|    FP_ARITH_INST_RETIRED_SCALAR_DOUBLE   |   PMC1  |          11 |
| FP_ARITH_INST_RETIRED_256B_PACKED_DOUBLE |   PMC2  |  4294967000 |
| FP_ARITH_INST_RETIRED_512B_PACKED_DOUBLE |   PMC3  |           0 |
+------------------------------------------+---------+-------------+

+----------------------+--------------+
|        Metric        |  HWThread 0  |
+----------------------+--------------+
|  Runtime (RDTSC) [s] |       3.1737 |
| Runtime unhalted [s] |       3.1646 |
|      Clock [MHz]     |    2593.5378 |
|          CPI         |       0.6341 |
|     DP [MFLOP/s]     |    5413.2582 |
|   AVX DP [MFLOP/s]   |    5413.2582 |
|  AVX512 DP [MFLOP/s] |            0 |
|   Packed [MUOPS/s]   |    1353.3145 |
|   Scalar [MUOPS/s]   | 3.466024e-06 |
|  Vectorization ratio |     100.0000 |
+----------------------+--------------+
```
and:
```
--------------------------------------------------------------------------------
CPU name:	Intel(R) Xeon(R) Gold 6240 CPU @ 2.60GHz
CPU type:	Intel Cascadelake SP processor
CPU clock:	2.59 GHz
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
Region Multiply ijk, Group 1: DATA
+-------------------+------------+
|    Region Info    | HWThread 0 |
+-------------------+------------+
| RDTSC Runtime [s] |  28.748650 |
|     call count    |          1 |
+-------------------+------------+

+-----------------------------+---------+-------------+
|            Event            | Counter |  HWThread 0 |
+-----------------------------+---------+-------------+
|      INSTR_RETIRED_ANY      |  FIXC0  | 51581610000 |
|    CPU_CLK_UNHALTED_CORE    |  FIXC1  | 74311320000 |
|     CPU_CLK_UNHALTED_REF    |  FIXC2  | 74317730000 |
|  MEM_INST_RETIRED_ALL_LOADS |   PMC0  | 17184080000 |
| MEM_INST_RETIRED_ALL_STORES |   PMC1  |     4194930 |
+-----------------------------+---------+-------------+

+----------------------+------------+
|        Metric        | HWThread 0 |
+----------------------+------------+
|  Runtime (RDTSC) [s] |    28.7487 |
| Runtime unhalted [s] |    28.7053 |
|      Clock [MHz]     |  2588.5403 |
|          CPI         |     1.4407 |
|  Load to store ratio |  4096.3926 |
+----------------------+------------+

Region Multiply ikj, Group 1: DATA
+-------------------+------------+
|    Region Info    | HWThread 0 |
+-------------------+------------+
| RDTSC Runtime [s] |   3.177819 |
|     call count    |          1 |
+-------------------+------------+

+-----------------------------+---------+-------------+
|            Event            | Counter |  HWThread 0 |
+-----------------------------+---------+-------------+
|      INSTR_RETIRED_ANY      |  FIXC0  | 12943640000 |
|    CPU_CLK_UNHALTED_CORE    |  FIXC1  |  8210681000 |
|     CPU_CLK_UNHALTED_REF    |  FIXC2  |  8210873000 |
|  MEM_INST_RETIRED_ALL_LOADS |   PMC0  |  4299164000 |
| MEM_INST_RETIRED_ALL_STORES |   PMC1  |  2147484000 |
+-----------------------------+---------+-------------+

+----------------------+------------+
|        Metric        | HWThread 0 |
+----------------------+------------+
|  Runtime (RDTSC) [s] |     3.1778 |
| Runtime unhalted [s] |     3.1717 |
|      Clock [MHz]     |  2588.7030 |
|          CPI         |     0.6343 |
|  Load to store ratio |     2.0020 |
+----------------------+------------+
```
Notice that the compiler is not able to vectorize the $`(i,j,k)`$ kernel without futher help, so the double precision operations are reported as scalar, while the $`(i,k,j)`$ kernel is fully vectorized with AVX instructions. This instruction set uses 256kB registers instead of 64kB ones, so we can expect a 4x factor in the vector stride (a stride of 32kB instead of 8B). 

> :warning: If you repeat this procedure on `miriel`, please keep in mind that not all FLOP counters are available on Intel Haswell (see the discussion at https://github.com/RRZE-HPC/likwid/wiki/FlopsHaswell). However, when the compiler is able to vectorize with the AVX instruction set, relevant information can be collected with the `FLOPS_AVX` group.

Having a look at the counters for the Cascadelake/SkylakeX architecture at https://github.com/RRZE-HPC/likwid/blob/master/groups/skylakeX/DATA.txt, and remembering that each instruction will handle 8 B (a double takes 64 bits), we can deduce the right counters to compute the operational intensity:
```math
I = \frac{ DP\ [MFLOP/s] \times 10^6 \times Runtime\ (RDTSC)\ [s] } { 8 B \times ( MEM\_INST\_RETIRED\_ALL\_LOADS + MEM\_INST\_RETIRED\_ALL\_STORES ) }
```

This will give an intensity of:
- 0.125 for the $`(i,j,k)`$ kernel, which is consistent with the predicted one;
- 0.3333 for the $`(i,k,j)`$ kernel, which is 4 times what we predicted (0.083333); anyway we have to consider that this kernel is vectorized with a 4x stride, so this is again consistent. The intensity will be divided by 4 in the roofline plot to get a representation consistent with the one usually offered Intel Advisor.

The roofline plot can be produced with the Gnuplot script [plot_roofline.pg](plot_roofline.pg), which needs a "perfs[suffix]" file containing one row per kernel, with a label, the operational intensity, the FLOP/s (in MFLOP/s) and the vector stride. For example, our "perfsBora" file would contain:
```
"ikj"   0.3331   5413.2582   4
"ijk"   0.125   599.9424   1
```

Once inside the Gnuplot command line, do:
```
fileSuffix="Bora"
load "plot_roofline.pg"
```
and here the result:

![roofline_bora](images/roofline.png)

More information about cache usage can be obtained for example with the performance groups `L2CACHE`, `L3CACHE` or `CYCLE_ACTIVITY`:
```
likwid-perfctr -m -C S0:0 -g L2CACHE ./program_loop
likwid-perfctr -m -C S0:0 -g L3CACHE ./program_loop
likwid-perfctr -m -C S0:0 -g CYCLE_ACTIVITY ./program_loop
```

## Conclusion
The roofline analysis allows to interpret the performances in terms of computations and data accesses. By showing a feasibility region for actual performances, and distinguishing a "compute-bound" from a "memory-bound" zone, it gives some guidance on how to further improve performances.
In any case this analysis has to be complemented with a cache usage analysis, but as a rule of thumb:
- a compute-bound program would primarily benefit from optimizations aimed at increasing its FLOP/s rate; this can also be achieved by changing its operational intensity, but with its FLOPS/s rate in mind;
- a memory-bound program would primarily benefit from optimizations aimed at increasing its operational intensity (if a kernel is on a memory roof, and it is then moved right in the plot, it can probably perform better since it is not blocked by the memory bandwidth anymore).

Increasing the operational intensity typically involves changing the data access patterns. The shown example is quite simple, but for more complex memory-bound applications there would be more advanced optimizations techniques to explore, like data _tiling_ to fit the data in specific cache levels.
Similarly, techniques to improve compute-bound applications have not been investigated here, with SIMD parallelism and vectorization among them.


# References
1. Ulrich Drepper, _"What every programmer should know about memory"_, full PDF version from the author's website https://www.akkadia.org/drepper/cpumemory.pdf (original part 1 https://lwn.net/Articles/250967/ and part 2 https://lwn.net/Articles/252125/)
1. Karl Rupp, _"https://www.karlrupp.net/2018/02/42-years-of-microprocessor-trend-data/"_, https://www.karlrupp.net/2018/02/42-years-of-microprocessor-trend-data/
1. Samuel Williams, Andrew Waterman, and David Patterson, _"Roofline: An Insightful Visual Performance Model
for Floating-Point Programs and Multicore Architectures"_, https://people.eecs.berkeley.edu/~kubitron/cs252/handouts/papers/RooflineVyNoYellow.pdf
1. Williams, S., Waterman, A., & Patterson, D. A. (2009), _"Roofline: An Insightful Visual Performance Model for Multicore Architectures"_, https://zenodo.org/records/1236156
1. Bei Wang, _"Performance and vectorization"_, https://tigress-web.princeton.edu/~jdh4/IntroToPerfVect_2021FallBreakWorkshop.pdf
1. S. Williams, _"Introduction to the roofline model"_ ([slides](https://crd.lbl.gov/assets/Uploads/roofline-intro.pdf))
1. L. A. Barba, R. Yokota, _"How Will the Fast Multipole Method Fare in the Exascale Era?"_ (SIAM newsjournal volume 46, number 6 [at the publisher website](https://www.siam.org/media/5rkpwdhe/sn_july-august2013.pdf), [image at the author website](https://lorenabarba.com/wp-content/uploads/2013/08/roofline.png))
1. Intel Advisor Guide, https://www.intel.com/content/dam/develop/external/us/en/documents/advisor-user-guide.pdf
1. LIKWID tools, https://hpc.fau.de/research/tools/likwid/
1. LIKWID wiki, https://github.com/RRZE-HPC/likwid/wiki
1. LIKWID tutorial: empirical roofline model, https://github.com/RRZE-HPC/likwid/wiki/Tutorial%3A-Empirical-Roofline-Model
1. Thomas Röhl, _"Performance Analysis with LIKWID"_, https://blog.rwth-aachen.de/hpc_import_20210107/attachments/20056127/24117298.pdf
1. _"Why no architecture has L1 or L1CACHE performance groups?"_, https://github.com/RRZE-HPC/likwid/issues/538
1. SLURM scheduler documentation https://slurm.schedmd.com/documentation.html
1. Environment Modules https://modules.readthedocs.io/en/latest/
1. GNU Guix https://guix.gnu.org/
1. Advanced Bash-Scripting Guide, https://tldp.org/LDP/abs/html/

[![License: CC BY 4.0](https://licensebuttons.net/l/by/4.0/80x15.png)](https://creativecommons.org/licenses/by/4.0/)
